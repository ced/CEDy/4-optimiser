def affiche_cout(tab, nb_tab):
    """
    Affichage des différents éléments d'un tableau, séparés par une tabulation.

    tab : liste des éléments
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    if nb_tab != 0:
        for e in tab:
            print(e, end="\t"*(nb_tab - int(len(e)/15)))
        print()


def affiche_cout_km(les_couts, les_unites_couts, nb_tab):
    """
    Affichage des coûts de déplacement.

    les_couts : liste des coûts kilométriques pour un mode de déplacement
    les_unites_couts : liste des unités associées aux coûts kilométriques
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    indice = 0
    for un_cout in les_couts:
        str_cout = les_unites_couts[indice].format(round(abs(un_cout), 1))
        if nb_tab == 0:
            fin = "\n"
        else:
            fin = "\t"*(nb_tab - int(len(str_cout)/15))
        print(str_cout, end=fin)
        indice = indice + 1
    if nb_tab != 0:
        print()


def calcule_cout_km(les_couts, km):
    """
    Calcule les coûts de déplacement.

    les_couts : liste des coûts pour 1 km d'un mode de déplacement
    km : distance entre le domicile et le lieu de travail

    retourne une liste des coûts kilométriques liés à un mode de déplacement
    """
    res = list(range(len(les_couts)))   # Initialiser la liste des résultats
    indice = 0
    for un_cout in les_couts:
        res[indice] = round(un_cout * km, 1)
        indice = indice + 1
    return res


def compare_cout_km(les_couts_1, les_couts_2):
    """
    Compare les coûts de déplacement.

    les_couts_1 : liste des coûts calculés pour le premier mode de déplacement
    les_couts_2 : liste des coûts calculés pour le second mode de déplacement

    retourne une liste contenant les coûts kilométriques comparés
    """
    diff = list(range(len(les_couts_1)))  # Initialiser la liste des résultats
    for indice in range(len(les_couts_1)):
        diff[indice] = les_couts_2[indice] - les_couts_1[indice]
    return diff


def choix_entier(message):
    """
    Demande à l'utilisateur de saisir un entier.

    message : question posée à l'utilisateur

    retourne un entier
    """
    return int(input(message))


def choix_deplacement(dico, km, cout_km, dep, choix_utilisateur):
    """
    Demande à l'utilisateur de saisir son mode de déplacement.

    dico : dictionnaire contenant la définition des en-têtes et des menus
    km : distance entre le domicile et le lieu de travail (entier)
    cout_km : dictionnaire contenant des listes de coûts pour les déplacements
    dep : entier représentant l'itération sur le mode de déplacement
    choix_utilisateur : dictionnaire mémorisant les choix des l'utilisateur

    retourne une liste des coûts kilométriques liés à un mode de déplacement
    """
    val_dep = choix_entier(dico['deplacement'].format(dico[dep]))
    if val_dep == 1:
        choix_utilisateur[dep] = dico['train']
        return calcule_cout_km(cout_km['train'], km)
    elif val_dep == 2:
        choix_utilisateur[dep] = dico['velo']
        return calcule_cout_km(cout_km['velo'], km)
    elif val_dep == 3:
        choix_utilisateur[dep] = dico['voiture']
        return calcule_cout_km(cout_km['voiture'], km)
    elif val_dep == 4:
        exit(0)


def run(dico, sep, cout_km, nb_tab):
    """
    Boucle principale de l'application. Affiche l'en-tête et les menus.

    dico : dictionnaire contenant la définition des en-têtes et des menus
    sep : chaîne de caractères servant de ligne de séparation
    cout_km : dictionnaire contenant des listes de coûts pour les déplacements
    nb_tab : entier représentant le nombre de tabulations à afficher

    pas de valeur de retour
    """
    while True:
        choix = {}          # initialiser les choix de l'utilisateur
        print(sep)
        print(dico['app'])
        print(sep)
        km = choix_entier(dico['distance'])
        if km == 0:
            break
        print(sep)
        # initialiser la liste des coûts pour le premier mode de déplacement
        mes_couts_dep_1 = choix_deplacement(dico, km, cout_km, 1, choix)
        print(sep)
        # initialiser la liste des coûts pour le second mode de déplacement
        mes_couts_dep_2 = choix_deplacement(dico, km, cout_km, 2, choix)
        print(sep)
        # comparer les coûts des deux modes de déplacement
        diff_couts = compare_cout_km(mes_couts_dep_1, mes_couts_dep_2)
        taille = nb_tab + 1
        print(" ", end="\t"*taille)
        affiche_cout(cout_km['lab_titres'], nb_tab)
        print(dico['choix_1'].format(choix[1]))
        print(" ", end="\t"*taille)
        affiche_cout_km(mes_couts_dep_1, cout_km['lab_unites'], nb_tab)
        print(dico['choix_2'].format(choix[2]))
        print(" ", end="\t"*taille)
        affiche_cout_km(mes_couts_dep_2, cout_km['lab_unites'], nb_tab)
        if diff_couts[0] < 0:
            affiche_cout_km(diff_couts, cout_km['lab_dep'], 0)
        else:
            affiche_cout_km(diff_couts, cout_km['lab_eco'], 0)


# Définition des variables globales
dico_fr = {
    'app': """\
Calculette : Eco-déplacements.
Calculez l'impact de vos déplacements quotidiens
sur l'environnement et vos dépenses""",
    'distance': """\
Quelle est distance entre le domicile et le lieu de travail ? """,
    'deplacement': """\
Choisir le {} mode de déplacement :
1 - train
2 - vélo
3 - voiture
4 - quitter
Mon choix : """,
    1: "premier",
    2: "second",
    'train': "le train",
    'velo': "le vélo",
    'voiture': "la voiture",
    'choix_1': 'Je choisis {}',
    'choix_2': 'plutôt que {}',
}
dico_cout = {
    'train': (14.62, 9.26),
    'velo': (0, 0),
    'voiture': (129.62, 50.65),
    'lab_titres': ("Effet de serre", "Énergie"),
    'lab_unites': ("{} kg eq. CO2", "{} l eq. pétrole"),
    'lab_eco': ("J'évite {} kg eq. CO2 par an",
                "Je consomme {} litres eq. pétrole en moins par an"),
    'lab_dep': ("J'émets {} kg eq. CO2 en plus par an",
                "Je consomme {} litres eq. pétrole en plus par an"),
}
ligne = "#" * 70
nb_tab = 2

# programme principal
run(dico_fr, ligne, dico_cout, nb_tab)
