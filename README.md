**Rappel** : Bien que la calculette de l'Ademe présente une interface graphique (GUI : Graphic User Interface), nous allons dans un premier temps, nous concentrer sur un programme en ligne de commandes (CLI : Command Line Interface).
**Écrire un programme CLI**, par rapport à un un programme GUI, présente de nombreux avantages, ne serait-ce que l'économie de moyens nécessaires au fonctionnement.
Un programme CLI sera surtout **beaucoup plus facile à écrire** dans un premier temps car nous n'aurons pas à interagir avec le serveur graphique et le gestionnaire de fenêtres et autres composants graphiques.

## Notre mission

Maintenant que notre application remplit à minimum le cahier des charges,
nous allons essayer d'optimiser notre application, c'est à dire
d'optimiser le code, de rendre certains traitements moins gourmands en
ressource, d'améliorer le dialogue homme-machine, de mieux gérer les données.
